import json
import requests

class HookApi:
    auth_key = None
    api_path = 'https://api.hooktheory.com/v1/'

    # Load Auth Data
    def __load_auth_data(self, loc='data/auth.json'):
        f = open(loc)
        data = json.load(f)
        f.close()
        return data

    def __header(self):
        return {"Accept": "application/json", "Content-Type": "application/json",
            "Authorization" : "Bearer " + self.auth_key}

    def is_connected(self):
        return not self.auth_key is None

    # Get Auth Key
    def get_auth_key(self):
        if not self.is_connected():
            resp = requests.post(self.api_path + 'users/auth', self.__load_auth_data())
            resp = json.loads(resp.text)
            if resp['activkey'] is None:
                raise NameError("Conncetion error")
            return resp['activkey']
        else:
            return self.auth_key

    def connect(self):
        self.auth_key = self.get_auth_key()

    def get_nodes(self, path=[]):
        params = ''
        if len(path) > 0:
            params = '?cp=' + ",".join(map(lambda x: str(x), path))
        resp = requests.get(self.api_path + 'trends/nodes' + params, headers=self.__header())
        try:
            return json.loads(resp.text)
        except:
            print("Bad response: " + str(path) + "" + resp.text)
            return []

api = HookApi()