import json

def load_chord_map():
    f = open("data/chord_index.json", "r")
    chord_map =json.load(f)["chord_map"]
    f.close()
    return chord_map

def load_chord_names_map():
    f = open("data/chord_names_map.json", "r")
    chord_map =json.load(f)["chord_names_map"]
    f.close()
    return chord_map

def create_chord_map():
    # Get chord ids
    f = open("data/chord_ids.json", "r")
    chord_ids = json.load(f)["chord_ids"]
    f.close()

    # Create chord map
    chord_map = {}
    index = 0
    for chord_id in chord_ids:
        chord_map[chord_id] = index
        index += 1
    
    # Write to file
    f = open("data/chord_index.json", "w")
    f.write(json.dumps({"chord_map": chord_map}))
    f.close()

def append_chord_ids(id):
    f = open("data/chord_ids.json", "r")
    chord_ids =json.load(f)["chord_ids"]
    f.close

    if not id in chord_ids:
        chord_ids.append(id)
        f = open("data/chord_ids.json", "w")
        f.write(json.dumps({"chord_ids": chord_ids}))
        f.close()
        raise NameError("Encountered new chord id, please rebuild model")



class Chord:
    val_id = None
    text_id = None
    prob = 0

    def __init__(self, obj):
        self.val_id = obj["chord_ID"]
        self.text_id = obj["chord_HTML"]
        self.prob = obj["probability"]