import numpy as np
import json
import time

from scripts.chord import append_chord_ids, load_chord_map, Chord
from scripts.hook_api import api
from random import random


def build_model(path = "data/model.json"):
    chord_map = load_chord_map()
    api.connect()

    # Initialize model array
    chord_len = len(chord_map)
    model = np.zeros((chord_len, chord_len))
    chord_html_names = {}
    # entry = np.zeros(chord_len)

    # Retrieve vales for model
    index = 1
    for chord in chord_map.keys():
        print(str(index) + "/" + str(chord_len), end="\r") # Progress tracker
        index += 1
        resp = api.get_nodes([chord])
        for obj in resp:
            try:
                chord_obj = Chord(obj)
            except:
                print(obj)
                print(resp)
            try:
                chord_map[chord_obj.val_id]
            except:
                try:
                    append_chord_ids(chord_obj.val_id)
                except:
                    print("Encountered new chord id, please rebuild model: " + chord_obj.val_id)
            else:
                model[chord_map[chord]][chord_map[chord_obj.val_id]] = chord_obj.prob
                # Add chord html names
                try:
                    chord_html_names[chord_obj.val_id]
                except:
                    chord_html_names[chord_obj.val_id] = chord_obj.text_id

        time.sleep(1) # Prevent exceeding request rate limit
        # I am aware there is a way to handle this built into the api
        # but I am lazy and this is not a frequent task

    # Write model to file
    f = open(path, "w")
    f.write(json.dumps({"model": model.tolist()}))
    f.close()

    # Write chord names to file
    f = open("data/chord_names_map.json", "w")
    f.write(json.dumps({"chord_names_map": chord_html_names}))
    f.close()

def load_model(path = "data/model.json"):
    # Read model from file
    f = open(path, "r")
    model = np.matrix(json.load(f)["model"])
    f.close()
    return model

def select_next_chord(model, chord_map, prev):
    prob_list = model[chord_map[prev]].tolist()[0]
    total_prob = sum(model[chord_map[prev]].tolist()[0])
    rand_val = random() * total_prob
    # print(prob_list)
    # print(total_prob)
    # print(rand_val)
    index = -1
    while rand_val > 0:
        index += 1
        rand_val = rand_val - prob_list[index]
    # print(index)
    chosen_chord = index
    return list(chord_map.keys())[list(chord_map.values()).index(index)]

def shortest_path(model, chord_map, source, dest):
    pass
    dists = [1] * len(chord_map.keys())
    unchecked_nodes = set(chord_map.keys())
    all_nodes = chord_map.keys()
    next_node = source
    while len(unchecked_nodes) > 0:
        # Remove node from unchecked nodes and find next node
        unchecked_nodes.remove(next_node)
        # Calculate new dists (unchecked nodes)
        for chord in unchecked_nodes:
            new_dist = dists[chord_map[next_node]] * model[chord_map[next_node], chord_map[chord]]
            if dists[chord_map[chord]] < new_dist or dists[chord_map[chord]] == 1:
                dists[chord_map[chord]] = new_dist
        # Find next node
        if len(unchecked_nodes) > 0:
            temp = 0
            for chord in unchecked_nodes:
                if temp < dists[chord_map[chord]]:
                    temp = dists[chord_map[chord]]
                    next_node = chord
            if next_node not in unchecked_nodes:
                break
    return dists[chord_map[dest]]
        
    

