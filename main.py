from scripts.hook_api import api
import scripts.chord as Chord
import scripts.model as Model

model = Model.load_model()

n_iters = 5
chords = ["1"]
chord_map = Chord.load_chord_map()
chord_names_map = Chord.load_chord_names_map()

while not chords[-1] == "1" or len(chords) <= 1:
    # Choose next chord
    next = Model.select_next_chord(model, chord_map, chords[-1])
    print(chords[-1] + " : " + str(Model.shortest_path(model, chord_map, chords[-1], "1")))
    chords.append(next)

print(list(map(lambda x: chord_names_map[x], chords)))

top_html = """<html>
<head>
<title>Title</title>
</head>
<body>
<h2>Chord Progression</h2>"""
chords_html = "".join(list(map(lambda x: "<p>" + chord_names_map[x] + "</p>", chords)))
bottom_html = """
</body>
</html> """


f = open("chord_progression.html", "w")
f.write(top_html + chords_html + bottom_html)
f.close()