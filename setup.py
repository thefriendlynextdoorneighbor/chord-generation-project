import scripts.chord as Chord
import scripts.model as Model
from scripts.hook_api import api
from os.path import exists
import json

auth_path = "data/auth.json"

if not exists(auth_path):
    f = open(auth_path, "w")
    f.write(json.dumps({"username": "your_user", "password": "your_pass"}))
    f.close()
    print("Please enter your authentication information in " + auth_path + " then rerun this script")
else:
    print("Building chord map")
    Chord.create_chord_map()
    print("Building model (this may take a while)")
    Model.build_model()
    print("All set up")